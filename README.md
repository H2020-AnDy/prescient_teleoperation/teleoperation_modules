# Teleoperation Modules

A collection of modules for the teleoperation of humanoid robots.
It implements the motion-retargeting from the Xsens motion-capture system to the robot. Teleoperation with prediction of intention and signal interruption.


## What you can find in here

- [retargeting](https://gitlab.inria.fr/locolearn/teleoperation-modules/tree/master/modules/retargeting) module: it takes the Xsens data as input and generates corresponding feasible values for the robot

- [promp_predictor](https://gitlab.inria.fr/locolearn/teleoperation-modules/tree/master/modules/promp_predictor) module: built upon the library [promp](https://gitlab.inria.fr/H2020-AnDy/promp)

- [utility/xsensDataplayer](https://gitlab.inria.fr/lpenco/teleoperation-modules/tree/master/utility/xsensDataPlayer): A folder with some recorded Xsens sequences that can be played with yarpdataplayer 

- [utility/prompDemo](https://gitlab.inria.fr/locolearn/teleoperation-modules/tree/master/utility/prompDemo): A folder with many recordings of human whole-body postures (i.csv i=1,2,...) and/or body segments poses  (pi.csv i=1,2,...) for several motions. With these you can train and play with your own ProMPs. Have fun!


## Software requirements

On Ubuntu 16.04:
* ROS
and/or
* YARP 


## Installation with ROS

```
git clone https://gitlab.inria.fr/lpenco/teleoperation_modules.git
cd ~/catkin_ws
catkin_make
```

## Installation with YARP

```
git clone https://gitlab.inria.fr/lpenco/teleoperation_modules.git
git checkout master_yarp
mkdir build && cd build
ccmake ..
```
choose what modules compile

configure and generate
```
make
```


## How to run the modules in ROS

- Run ros master
```
roscore
```

- To run a node (i.e. *promp_predictor* or *xsens_retargeting*)
```
rosrun teleoperation_modules [node_name]
```

## How to run the modules in YARP

- Be sure you have installed everything and you are currently on the *master_yarp* branch

- Run a yarp server if one is not already running.
```
yarpserver
```

- If there is already a server running, check the ip and the namespace of the server (*yarpwhere* in the machine where the server is running)

- set the proper yarp namespace in your machine
```
yarp namespace <namespace-server>
```

- and overwrite the yarp conf file
```
yarp conf <ip-address-server> 10000
```

- Run the yarp node for the teleoperation
```
yarprun --server /teleop
```

- Run the yarp GUI
```
yarpmanager
```

- Open folder *.../teleoperation-modules/etc/scripts*

- Run the modules you want and connect



